@extends('layouts.main')

@section('container')

<h3 class="text-center mb-4">Narkotika Golongan 2</h3>

<div class="col-4">
    @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        @endif
    </div>

<form action="/narkotika-golongan-2/show" method="get" class="col-4">
    <a href="{{ auth()->user()->role == 'member' ? '/' : '/narkotika-golongan-2' }}" class="badge bg-primary text-decoration-none">kembali</a>
    <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Search.." name="keyword" value="{{ request('keyword') }}">
        <button class="btn btn-primary" type="submit">Search</button>
        <a href="/narkotika-golongan-2/show" class="btn btn-danger">Clear</a>
    </div>
</form>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nama Resmi</th>
        <th scope="col">Istilah</th>
        <th scope="col">Kandungan</th>
        <th scope="col">Status</th>
        <th scope="col">Landasan</th>
        <th scope="col">Link</th>
        @if(auth()->user()->role == 'admin')
        <th scope="col">Action</th>
        @endif
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $d)
        <tr>
          <th scope="row">{{ $d->nama_resmi }}</th>
          <td>{{ $d->istilah }}</td>
          <td>{{ $d->kandungan }}</td>
          <td>{{ $d->status }}</td>
          <td>{{ $d->landasan }}</td>
          <td><a href="{{ $d->link }}" target="_blank">{{ $d->link }}</a></td>
          @if(auth()->user()->role == 'admin')
          <td>
              <a href="/narkotika-golongan-2/edit/{{ $d->id }}"><span class="badge bg-warning">edit</span></a>
              <a href="/narkotika-golongan-2/delete/{{ $d->id }}"><span class="badge bg-danger" onclick="return confirm('are you sure?')">delete</span></a>
          </td>
          @endif
        </tr>
        @endforeach
    </tbody>
  </table>
  <div class="d-flex justify-content-center">
      {!! $data->links() !!}
  </div>
@endsection
