@extends('layouts.main')

@section('container')
<a href="/" class="badge bg-primary text-decoration-none">kembali</a>
<h3 class="text-center mb-3">List New User</h3>

<div class="col-4">
    @if(session()->has('accept'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('accept') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        @endif
    </div>

<div class="col-4">
    @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        @endif
    </div>

<table class="table">
    <thead>
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Active Account</th>
        <th scope="col">Role</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($data as $d)
        <tr>
          <th scope="row">{{ $d->name }}</th>
          <td>{{ $d->email }}</td>
          <td>{{ $d->is_active == 0 ? 'belum aktif' : 'aktif' }}</td>
          <td>{{ $d->role }}</td>
          <td>
              <a onclick="return confirm('Anda yakin?')" href="/new-user/accept/{{ $d->id }}" class="badge bg-success text-decoration-none">accept</a>
              <a onclick="return confirm('Anda yakin?')" href="/new-user/delete/{{ $d->id }}" class="badge bg-danger text-decoration-none">delete</a>
          </td>
        </tr>
        @endforeach
    </tbody>
  </table>
@endsection
