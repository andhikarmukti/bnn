@extends('layouts.main')

@section('container')
<div class="col-4">
    <a href="{{ url()->previous() }}">kembali</a>
    <form class="mt-5" method="post" action="/narkotika-golongan-3">
        @csrf
        @method('PUT')
        <input type="hidden" class="form-control" id="id" name="id" value="{{ $data['id'] }}">
        <div class="mb-3">
          <label for="nama_resmi" class="form-label">Nama Resmi</label>
          <input type="text" class="form-control" id="nama_resmi" name="nama_resmi" value="{{ $data['nama_resmi'] }}">
        </div>
        <div class="mb-3">
          <label for="istilah" class="form-label">Istilah</label>
          <input type="text" class="form-control" id="istilah" name="istilah" value="{{ $data['istilah'] }}">
        </div>
        <div class="mb-3">
          <label for="kandungan" class="form-label">Kandungan</label>
          <input type="text" class="form-control" id="kandungan" name="kandungan" value="{{ $data['kandungan'] }}">
        </div>
        <div class="mb-3">
          <label for="status" class="form-label">Status</label>
          <input type="text" class="form-control" id="status" name="status" value="{{ $data['status'] }}">
        </div>
        <div class="mb-3">
          <label for="landasan" class="form-label">Landasan</label>
          <input type="text" class="form-control" id="landasan" name="landasan" value="{{ $data['landasan'] }}">
        </div>
        <div class="mb-3">
          <label for="link" class="form-label">Link</label>
          <input type="text" class="form-control" id="link" name="link" value="{{ $data['link'] }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>
@endsection
