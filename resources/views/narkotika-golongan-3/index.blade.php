@extends('layouts.main')

@section('container')
<div class="col-4">
    <a class="mb-2 badge bg-primary text-decoration-none" href="/">kembali</a>
    <br>
    <h3>Narkotika Golongan 3</h3>
    <a class="btn btn-primary" href="/narkotika-golongan-3/show" class="badge bg-primary text-decoration-none">Show All Data</a>

    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif

    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif

    @if(session()->has('edit'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('edit') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif

    <form class="mt-5" method="post" action="narkotika-golongan-3">
        @csrf
        <div class="mb-3">
          <label for="nama_resmi" class="form-label">Nama Resmi</label>
          <input type="text" class="form-control" id="nama_resmi" name="nama_resmi">
        </div>
        <div class="mb-3">
          <label for="istilah" class="form-label">Istilah</label>
          <input type="text" class="form-control" id="istilah" name="istilah">
        </div>
        <div class="mb-3">
          <label for="kandungan" class="form-label">Kandungan</label>
          <input type="text" class="form-control" id="kandungan" name="kandungan">
        </div>
        <div class="mb-3">
          <label for="status" class="form-label">Status</label>
          <input type="text" class="form-control" id="status" name="status">
        </div>
        <div class="mb-3">
          <label for="landasan" class="form-label">Landasan</label>
          <input type="text" class="form-control" id="landasan" name="landasan">
        </div>
        <div class="mb-3">
          <label for="link" class="form-label">Link</label>
          <input type="text" class="form-control" id="link" name="link">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
