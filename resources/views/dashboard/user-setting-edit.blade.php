@extends('layouts.main')

@section('container')
    <a href="/user-setting">kembali</a>

    <h3 class="mt-5 mb-3">{{ $user->name }}</h3>
    <div class="col-6">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/user-setting" method="POST">
            @csrf
            <input type="hidden" value="{{ $user->id }}" name="id" id="id">
            <div class="mb-3">
              <label for="is_active" class="form-label">Activation</label>
              <select class="form-select" id="is_active" name="is_active">
                <option value="1" {{ $user->is_active === 1 ? 'selected' : '' }}>active</option>
                <option value="0" {{ $user->is_active === 0 ? 'selected' : '' }}>deactive</option>
              </select>
            </div>
            <div class="mb-3">
              <label for="role" class="form-label">Role</label>
              <select class="form-select" id="role" name="role">
                <option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>admin</option>
                <option value="member" {{ $user->role == 'member' ? 'selected' : '' }}>member</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
@endsection
