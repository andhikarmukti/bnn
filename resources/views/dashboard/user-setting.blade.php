@extends('layouts.main')

@section('container')
<a href="/dashboard">kembali</a>
    <h3 class="mt-5 text-center mb-4">User Setting</h3>

    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Activation</th>
            <th scope="col">Role</th>
            <th scope="col" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
              <th scope="row">{{ $user->name }}</th>
              <td class="{{ $user->is_active == 1 ? 'text-success' : 'text-danger' }}">{{ $user->is_active == 1 ? 'active' : 'deactive' }}</td>
              <td>{{ $user->role }}</td>
              <td class="text-center">
                  <a href="user-setting/edit/{{ $user->id }}" class="badge bg-warning text-decoration-none">edit</a>
                  <a href="#" class="badge bg-danger text-decoration-none">delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
@endsection
