@extends('layouts.main')

@section('container')
@if(auth()->user()->role == 'admin')
<div class="mb-3 mt-5">
    <a class="btn btn-primary position-relative" href="/new-user">New User
        @if($notif > 0)
        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger"> {{ $notif }} </span>
        @endif
    </a>
</div>
@endif
<div class="mb-3 mt-5">
    <a class="btn btn-primary" href="/narkotika-golongan-1">Narkotika Golongan 1</a>
</div>
<div class="mb-3">
    <a class="btn btn-info" href="/narkotika-golongan-2">Narkotika Golongan 2</a>
</div>
<div class="mb-3">
    <a class="btn btn-success" href="/narkotika-golongan-3">Narkotika Golongan 3</a>
</div>
<div class="mb-3">
    <a class="btn btn-warning" href="/narkotika-tingkat-lanjut">Narkotika Tingkat Lanjut</a>
</div>
<div class="mb-3">
    <form action="{{ route('logout') }}" method="post">
        @csrf
        <button type="submit" class="btn btn-danger" href="/narkotika-tingkat-lanjut">Logout</button>
    </form>
</div>
@endsection
