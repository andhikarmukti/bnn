<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NarkotikaGolongan1ApiController;
use App\Http\Controllers\NarkotikaGolongan2ApiController;
use App\Http\Controllers\NarkotikaGolongan3ApiController;
use App\Http\Controllers\NarkotikaTingkatLanjutApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Narkotika Golongan 1
Route::get('/ng-1-show', [NarkotikaGolongan1ApiController::class, 'show'])->middleware('api-auth');
Route::post('/ng-1-create', [NarkotikaGolongan1ApiController::class, 'store'])->middleware('api-auth');
Route::put('/ng-1-update', [NarkotikaGolongan1ApiController::class, 'update'])->middleware('api-auth');
Route::delete('/ng-1-delete', [NarkotikaGolongan1ApiController::class, 'destroy'])->middleware('api-auth');

// Narkotika Golongan 2
Route::get('/ng-2-show', [NarkotikaGolongan2ApiController::class, 'show'])->middleware('api-auth');
Route::post('/ng-2-create', [NarkotikaGolongan2ApiController::class, 'store'])->middleware('api-auth');
Route::put('/ng-2-update', [NarkotikaGolongan2ApiController::class, 'update'])->middleware('api-auth');
Route::delete('/ng-2-delete', [NarkotikaGolongan2ApiController::class, 'destroy'])->middleware('api-auth');

// Narkotika Golongan 3
Route::get('/ng-3-show', [NarkotikaGolongan3ApiController::class, 'show'])->middleware('api-auth');
Route::post('/ng-3-create', [NarkotikaGolongan3ApiController::class, 'store'])->middleware('api-auth');
Route::put('/ng-3-update', [NarkotikaGolongan3ApiController::class, 'update'])->middleware('api-auth');
Route::delete('/ng-3-delete', [NarkotikaGolongan3ApiController::class, 'destroy'])->middleware('api-auth');

// Narkotika Golongan Tingkat Lanjut
Route::get('/ntl-show', [NarkotikaTingkatLanjutApiController::class, 'show'])->middleware('api-auth');
Route::post('/ntl-create', [NarkotikaTingkatLanjutApiController::class, 'store'])->middleware('api-auth');
Route::put('/ntl-update', [NarkotikaTingkatLanjutApiController::class, 'update'])->middleware('api-auth');
Route::delete('/ntl-delete', [NarkotikaTingkatLanjutApiController::class, 'destroy'])->middleware('api-auth');
