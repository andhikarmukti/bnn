<?php

use App\Http\Controllers\DashboardController;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\NarkotikaGolongan1Controller;
use App\Http\Controllers\NarkotikaGolongan2Controller;
use App\Http\Controllers\NarkotikaGolongan3Controller;
use App\Http\Controllers\NarkotikaTingkatLanjutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

// Pages
Route::get('/', [PagesController::class, 'home'])->middleware('auth');
Route::get('/new-user', [PagesController::class, 'newUser'])->middleware('auth', 'adminOnly');
Route::get('/new-user/accept/{id}', function () {
    User::where('id', request('id'))->update(['is_active' => TRUE]);
    return back()->with('accept', 'Berhasil accept');
})->middleware('auth', 'adminOnly');
Route::get('/new-user/delete/{id}', function () {
    User::destroy(request('id'));
    return back()->with('delete', 'Berhasil delete');
})->middleware('auth', 'adminOnly');

// narkotika golongan 1
Route::get('/narkotika-golongan-1', [NarkotikaGolongan1Controller::class, 'index'])->middleware('auth');
Route::get('/narkotika-golongan-1/show', [NarkotikaGolongan1Controller::class, 'show'])->middleware('auth');
Route::get('/narkotika-golongan-1/create', [NarkotikaGolongan1Controller::class, 'create'])->middleware('auth');
Route::get('/narkotika-golongan-1/edit/{id}', [NarkotikaGolongan1Controller::class, 'edit'])->middleware('auth');
Route::post('/narkotika-golongan-1', [NarkotikaGolongan1Controller::class, 'store'])->middleware('auth');
Route::put('/narkotika-golongan-1', [NarkotikaGolongan1Controller::class, 'update'])->middleware('auth');
Route::get('/narkotika-golongan-1/delete/{id}', [NarkotikaGolongan1Controller::class, 'destroy'])->middleware('auth');

// narkotika golongan 2
Route::get('/narkotika-golongan-2', [NarkotikaGolongan2Controller::class, 'index'])->middleware('auth');
Route::get('/narkotika-golongan-2/show', [NarkotikaGolongan2Controller::class, 'show'])->middleware('auth');
Route::post('/narkotika-golongan-2', [NarkotikaGolongan2Controller::class, 'store'])->middleware('auth');
Route::put('/narkotika-golongan-2', [NarkotikaGolongan2Controller::class, 'update'])->middleware('auth');
Route::get('/narkotika-golongan-2/delete/{id}', [NarkotikaGolongan2Controller::class, 'destroy'])->middleware('auth');
Route::get('/narkotika-golongan-2/edit/{id}', [NarkotikaGolongan2Controller::class, 'edit'])->middleware('auth');

// narkotika golongan 3
Route::get('/narkotika-golongan-3', [NarkotikaGolongan3Controller::class, 'index'])->middleware('auth');
Route::get('/narkotika-golongan-3/show', [NarkotikaGolongan3Controller::class, 'show'])->middleware('auth');
Route::put('/narkotika-golongan-3', [NarkotikaGolongan3Controller::class, 'update'])->middleware('auth');
Route::post('/narkotika-golongan-3', [NarkotikaGolongan3Controller::class, 'store'])->middleware('auth');
Route::get('/narkotika-golongan-3/delete/{id}', [NarkotikaGolongan3Controller::class, 'destroy'])->middleware('auth');
Route::get('/narkotika-golongan-3/edit/{id}', [NarkotikaGolongan3Controller::class, 'edit'])->middleware('auth');

// narkotika tingkat lanjut
Route::get('/narkotika-tingkat-lanjut', [NarkotikaTingkatLanjutController::class, 'index'])->middleware('auth');
Route::get('/narkotika-tingkat-lanjut/show', [NarkotikaTingkatLanjutController::class, 'show'])->middleware('auth');
Route::put('/narkotika-tingkat-lanjut', [NarkotikaTingkatLanjutController::class, 'update'])->middleware('auth');
Route::post('/narkotika-tingkat-lanjut', [NarkotikaTingkatLanjutController::class, 'store'])->middleware('auth');
Route::get('/narkotika-tingkat-lanjut/delete/{id}', [NarkotikaTingkatLanjutController::class, 'destroy'])->middleware('auth');
Route::get('/narkotika-tingkat-lanjut/edit/{id}', [NarkotikaTingkatLanjutController::class, 'edit'])->middleware('auth');

// dashboard
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth', 'adminOnly');

// User
Route::get('/user-setting', [DashboardController::class, 'user_setting'])->middleware('auth', 'adminOnly');
Route::get('/user-setting/edit/{id}', [DashboardController::class, 'user_setting_edit'])->middleware('auth', 'adminOnly');
Route::post('/user-setting', [DashboardController::class, 'user_setting_update'])->middleware('auth', 'adminOnly');
