<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNarkotikaTingkatLanjutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narkotika_tingkat_lanjuts', function (Blueprint $table) {
            $table->id();
            $table->string('nama_resmi');
            $table->string('istilah');
            $table->string('kandungan');
            $table->string('status');
            $table->string('landasan');
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narkotika_tingkat_lanjuts');
    }
}
