<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NarkotikaGolongan3;

class NarkotikaGolongan3ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_resmi = $request->nama_resmi;
        $istilah    = $request->istilah;
        $kandungan  = $request->kandungan;
        $status     = $request->status;
        $landasan   = $request->landasan;
        $link       = $request->link;

        if ($nama_resmi && $istilah && $kandungan && $status && $landasan && $link) {
            try {
                NarkotikaGolongan3::create([
                    'nama_resmi' => $nama_resmi,
                    'istilah'    => $istilah,
                    'kandungan'  => $kandungan,
                    'status'     => $status,
                    'landasan'   => $landasan,
                    'link'       => $link
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'successful insert data Narkotika Golongan 3'
                ]);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $narkotika_golongan_3 = NarkotikaGolongan3::all();

        if ($request->ng3_id) {
            $narkotika_tingkat_lanjut = NarkotikaGolongan3::find($request->ng3_id);
            return response()->json($narkotika_tingkat_lanjut, 200);
        }

        return response()->json($narkotika_golongan_3, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ng3_id     = $request->ng3_id;
        $nama_resmi = $request->nama_resmi;
        $istilah    = $request->istilah;
        $kandungan  = $request->kandungan;
        $status     = $request->status;
        $landasan   = $request->landasan;
        $link       = $request->link;

        if ($ng3_id && $nama_resmi && $istilah && $kandungan && $status && $landasan && $link) {
            try {
                NarkotikaGolongan3::find($ng3_id)->update([
                    'nama_resmi' => $nama_resmi,
                    'istilah'    => $istilah,
                    'kandungan'  => $kandungan,
                    'status'     => $status,
                    'landasan'   => $landasan,
                    'link'       => $link
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'successful update data Narkotika Golongan 3 with ID ' . $ng3_id
                ]);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ng3_id = $request->ng3_id;

        if ($ng3_id) {
            try {
                NarkotikaGolongan3::find($ng3_id)->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Successful delete data Narkotika Golongan 3 with ID ' . $ng3_id
                ], 200);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }
}
