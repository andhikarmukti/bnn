<?php

namespace App\Http\Controllers;

use App\Models\NarkotikaGolongan2;
use Illuminate\Http\Request;

class NarkotikaGolongan2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NarkotikaGolongan2::all();

        if (auth()->user()->role == 'member') {
            return redirect('/narkotika-golongan-2/show');
        }

        return view('narkotika-golongan-2.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NarkotikaGolongan2::create([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return back()->with('success', 'berhasil melakukan input data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan2  $narkotikaGolongan2
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = NarkotikaGolongan2::paginate(20)->withQueryString();

        if ($request->keyword) {
            $data = NarkotikaGolongan2::where('nama_resmi', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('istilah', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('kandungan', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('status', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('landasan', 'LIKE', '%' . $request->keyword . '%')
                ->paginate(20)->withQueryString();
        }

        return view('narkotika-golongan-2.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan2  $narkotikaGolongan2
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NarkotikaGolongan2::where('id', $id)->get()->toArray();

        return view('narkotika-golongan-2.edit', [
            'data' => $data[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NarkotikaGolongan2  $narkotikaGolongan2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        NarkotikaGolongan2::where('id', $request->id)->update([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return redirect('/narkotika-golongan-2')->with('edit', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NarkotikaGolongan2  $narkotikaGolongan2
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NarkotikaGolongan2::destroy($id);

        return back()->with('delete', 'Data berhasil dihapus');
    }
}
