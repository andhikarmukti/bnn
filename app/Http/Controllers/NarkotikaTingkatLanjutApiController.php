<?php

namespace App\Http\Controllers;

use App\Models\NarkotikaTingkatLanjut;
use Illuminate\Http\Request;

class NarkotikaTingkatLanjutApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_resmi = $request->nama_resmi;
        $istilah    = $request->istilah;
        $kandungan  = $request->kandungan;
        $status     = $request->status;
        $landasan   = $request->landasan;
        $link       = $request->link;

        if ($nama_resmi && $istilah && $kandungan && $status && $landasan && $link) {
            try {
                NarkotikaTingkatLanjut::create([
                    'nama_resmi' => $nama_resmi,
                    'istilah'    => $istilah,
                    'kandungan'  => $kandungan,
                    'status'     => $status,
                    'landasan'   => $landasan,
                    'link'       => $link
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'successful insert data Narkotika Tingkat Lanjut'
                ]);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $narkotika_tingkat_lanjut = NarkotikaTingkatLanjut::all();

        if ($request->ntl_id) {
            $narkotika_tingkat_lanjut = NarkotikaTingkatLanjut::find($request->ntl_id);
            return response()->json($narkotika_tingkat_lanjut, 200);
        }

        return response()->json($narkotika_tingkat_lanjut, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ntl_id     = $request->ntl_id;
        $nama_resmi = $request->nama_resmi;
        $istilah    = $request->istilah;
        $kandungan  = $request->kandungan;
        $status     = $request->status;
        $landasan   = $request->landasan;
        $link       = $request->link;

        if ($ntl_id && $nama_resmi && $istilah && $kandungan && $status && $landasan && $link) {
            try {
                NarkotikaTingkatLanjut::find($ntl_id)->update([
                    'nama_resmi' => $nama_resmi,
                    'istilah'    => $istilah,
                    'kandungan'  => $kandungan,
                    'status'     => $status,
                    'landasan'   => $landasan,
                    'link'       => $link
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'successful update data Narkotika Tingkat Lanjut with ID ' . $ntl_id
                ]);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ntl_id = $request->ntl_id;

        if ($ntl_id) {
            try {
                NarkotikaTingkatLanjut::find($ntl_id)->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Successful delete data Narkotika Golongan 3 with ID ' . $ntl_id
                ], 200);
            } catch (\Exception $e) {
                $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }
}
