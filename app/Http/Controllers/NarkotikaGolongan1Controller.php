<?php

namespace App\Http\Controllers;

use App\Models\NarkotikaGolongan1;
use Illuminate\Http\Request;

class NarkotikaGolongan1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NarkotikaGolongan1::all();

        if (auth()->user()->role == 'member') {
            return redirect('/narkotika-golongan-1/show');
        }

        return view('narkotika-golongan-1.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('narkotika-golongan-1.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NarkotikaGolongan1::create([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return back()->with('success', 'berhasil melakukan input data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan1  $narkotikaGolongan1
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = NarkotikaGolongan1::paginate(20)->withQueryString();

        if ($request->keyword) {
            $data = NarkotikaGolongan1::where('nama_resmi', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('istilah', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('kandungan', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('status', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('landasan', 'LIKE', '%' . $request->keyword . '%')
                ->paginate(20)->withQueryString();
        }

        return view('narkotika-golongan-1.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan1  $narkotikaGolongan1
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NarkotikaGolongan1::where('id', $id)->get()->toArray();

        return view('narkotika-golongan-1.edit', [
            'data' => $data[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NarkotikaGolongan1  $narkotikaGolongan1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        NarkotikaGolongan1::where('id', $request->id)->update([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return redirect('/narkotika-golongan-1')->with('edit', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NarkotikaGolongan1  $narkotikaGolongan1
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NarkotikaGolongan1::destroy($id);

        return back()->with('delete', 'Data berhasil dihapus');
    }
}
