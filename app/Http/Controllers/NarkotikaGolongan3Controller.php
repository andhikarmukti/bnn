<?php

namespace App\Http\Controllers;

use App\Models\NarkotikaGolongan2;
use App\Models\NarkotikaGolongan3;
use Illuminate\Http\Request;

class NarkotikaGolongan3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NarkotikaGolongan3::all();

        if (auth()->user()->role == 'member') {
            return redirect('/narkotika-golongan-3/show');
        }

        return view('narkotika-golongan-3.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NarkotikaGolongan3::create([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return back()->with('success', 'berhasil melakukan input data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan3  $narkotikaGolongan3
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = NarkotikaGolongan3::paginate(20)->withQueryString();

        if ($request->keyword) {
            $data = NarkotikaGolongan3::where('nama_resmi', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('istilah', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('kandungan', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('status', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('landasan', 'LIKE', '%' . $request->keyword . '%')
                ->paginate(20)->withQueryString();
        }

        return view('narkotika-golongan-3.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NarkotikaGolongan3  $narkotikaGolongan3
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NarkotikaGolongan3::where('id', $id)->get()->toArray();

        return view('narkotika-golongan-3.edit', [
            'data' => $data[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NarkotikaGolongan3  $narkotikaGolongan3
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        NarkotikaGolongan3::where('id', $request->id)->update([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return redirect('/narkotika-golongan-3')->with('edit', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NarkotikaGolongan3  $narkotikaGolongan3
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NarkotikaGolongan3::destroy($id);

        return back()->with('delete', 'Data berhasil dihapus');
    }
}
