<?php

namespace App\Http\Controllers;

use App\Models\NarkotikaTingkatLanjut;
use Illuminate\Http\Request;

class NarkotikaTingkatLanjutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = NarkotikaTingkatLanjut::all();

        if (auth()->user()->role == 'member') {
            return redirect('/narkotika-tingkat-lanjut/show');
        }

        return view('narkotika-tingkat-lanjut.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NarkotikaTingkatLanjut::create([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return back()->with('success', 'berhasil melakukan input data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NarkotikaTingkatLanjut  $narkotikaTingkatLanjut
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = NarkotikaTingkatLanjut::paginate(20)->withQueryString();

        if ($request->keyword) {
            $data = NarkotikaTingkatLanjut::where('nama_resmi', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('istilah', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('kandungan', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('status', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('landasan', 'LIKE', '%' . $request->keyword . '%')
                ->paginate(20)->withQueryString();
        }

        return view('narkotika-tingkat-lanjut.show', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NarkotikaTingkatLanjut  $narkotikaTingkatLanjut
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NarkotikaTingkatLanjut::where('id', $id)->get()->toArray();

        return view('narkotika-tingkat-lanjut.edit', [
            'data' => $data[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NarkotikaTingkatLanjut  $narkotikaTingkatLanjut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        NarkotikaTingkatLanjut::where('id', $request->id)->update([
            'nama_resmi' => $request->nama_resmi,
            'istilah' => $request->istilah,
            'kandungan' => $request->kandungan,
            'status' => $request->status,
            'landasan' => $request->landasan,
            'link' => $request->link,
        ]);

        return redirect('/narkotika-tingkat-lanjut')->with('edit', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NarkotikaTingkatLanjut  $narkotikaTingkatLanjut
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NarkotikaTingkatLanjut::destroy($id);

        return back()->with('delete', 'Data berhasil dihapus');
    }
}
