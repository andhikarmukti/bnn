<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\NarkotikaGolongan1;

class PagesController extends Controller
{
    public function home()
    {
        $notif = User::where('is_active', 0)->get()->count();

        return view('home', [
            'notif' => $notif
        ]);
    }

    public function newUser()
    {
        $data = User::where('is_active', 0)->get();

        return view('new-user', [
            'data' => $data
        ]);
    }
}
