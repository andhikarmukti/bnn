<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->token != '2a04Phk84VMwAsO7NpbQHL4XuhnmMlIC4gUjGpr6QOoeQ') {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad api-auth'
            ], 401);
        }
        return $next($request);
    }
}
