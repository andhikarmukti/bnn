<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NarkotikaGolongan2 extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
}
